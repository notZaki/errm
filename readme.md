# Readme

This is a preprint of the paper titled:

> An extended reference region model for DCE-MRI that accounts for plasma volume
> Zaki Ahmed & Ives R. Levesque
> NMR in Biomedicine. 10 May 2018. [DOI: 10.1002/nbm.3924](https://doi.org/10.1002/nbm.3924)

The manuscript preprint is available as an:

- [HTML version](https://notzaki.gitlab.io/errm/)
- [PDF version](https://notzaki.gitlab.io/errm/_main.pdf)

This is the submitted version prior to peer review, so there will likely be some differences between the content here and what was ultimately published. 
The post-peer-review version has a year long embargo. 